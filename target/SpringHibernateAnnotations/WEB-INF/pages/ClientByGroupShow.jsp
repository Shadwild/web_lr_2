<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 26.03.2018
  Time: 14:58
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/X.js"></script>
    <title>Список клиент-группа</title>
</head>
<body>
<div id="header">
    <p id="main"><font color="white">Записи/</font><font color=#bdc3c7>Спикок клиент-группа</font></p>
</div>
<table border=1>
    <tr>
        <td>ID-пользователя</td>
        <td>ID-группы</td>
    </tr>
    <c:forEach items="${clientByGroups}" var="clientByGroup" varStatus="loop">
        <tr onclick="nextPage(${loop.count},'ClientByGroupEdit')" id="${loop.count}">
            <td>${clientByGroups.clientId}</td>
            <td>${clientByGroups.groupId}</td>
        </tr>
    </c:forEach>
</table>
<a href=index.jsp><- Start page</a>
</body>
</html>