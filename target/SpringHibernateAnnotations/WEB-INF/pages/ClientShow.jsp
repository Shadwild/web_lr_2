<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 24.03.2018
  Time: 10:33
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--  <link rel="stylesheet" type="text/css" href="style.css">-->
   <!-- <script src="js/X.js"></script>-->
    <title>Пользователи</title>
</head>
<body>
<div id="header">
    <p id="main"><font color="white">Записи/</font><font color=#bdc3c7>Пользователи</font></p>
</div>
<table border=1>
    <tr>
        <td>ID-пользователя</td>
        <td>Логин</td>
        <td>Пароль</td>
    </tr>
    <c:forEach items="${clients}" var="client" varStatus="loop">
        <tr onclick="nextPage(${loop.count},'ClientEdit')" id="${loop.count}">
            <td>${client.clientId}</td>
            <td>${client.login}</td>
            <td>${client.password}</td>
        </tr>
    </c:forEach>
</table>
<a href=../../index.jsp><- Start page</a>
</body>


<style>
    a, p {
        font-family: Lucida Sans Unicode;
        font-size: 16px;
    }

    h3 {

        color: #343434;
        font-size: 22px;
        line-height: 40px;
        font-weight: 100;
        font-family: "PT Sans", Helvetica, Arial, sans-serif;
        letter-spacing: 1px;
    }

    #header {
        position: absolute;
        top: 0px;
        left: 130px;
        background: rgb(45, 45, 68);
        width: 90%;
        height: 7%;
        border-bottom: 3px inset #10498f;
    }

    #main {
        color: white;
        font-size: 22px;
        font-weight: 100;
        font-family: "PT Sans", Helvetica, Arial, sans-serif;
        letter-spacing: 1px;
        margin-left: 10px;
        margin-top: 10px;
    }

    input[type="text"], textarea {
        background-color: #AFCDE7;
        width: 100%;
        border: none;
        height: 100%;
        font-size: 18px;
        font-family: Hammersmith One;
    }

    input[type="radio"], button {
        font-size: 14px;
        margin: 3px;
        font-family: Lucida Sans Unicode;
    }

    inputbutton {
        font-size: 14px;
        margin: 5px;
        font-family: Lucida Sans Unicode;
    }

    select {
        width: 100%;
        border: none;
        height: 100%;
        font-size: 18px;
        font-family: Hammersmith One;
    }

    body {
        background: linear-gradient(to bottom, white, #6495ed);
        background-attachment: fixed;
    }

    table {
        font-family: Lucida Sans Unicode;
        border-collapse: collapse;
        color: #000000;
    }

    td {
        padding: 10px;
    }

    tr:nth-child(odd) {
        background: #AFCDE7;
    }

    tr:nth-child(even) {
        background: #D8E6F3;
    }

    body {
        margin-left: 12%;
        margin-top: 5%;
    }

    ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    #menu {
        position: absolute;
        top: 0px;
        left: 0px;
        position: fixed;
        width: 10%;
        height: 100%
    }

    #menu li a:link, #menu li a:visited {
        color: #FFF;
        display: block;
        background: #2079e6;
        border-right: 3px inset #10498f;
        border-top: 2px solid white;
        width: 130px;
        height: 130px;
    }

    #menu li a:hover {
        color: #FFF;
        background: #7aaff0;
        width: 130px;
        height: 130px;
        border-right: 3px inset #10498f;
        border-top: 2px solid white;
    }

    #menu li a {
        height: 100%;
        text-decoration: none;
    }

    #but {
        background: -webkit-gradient(linear, 0 0, 0 100%, from(#D0ECF4), to(#D0ECF4), color-stop(0.5, #10498f));
        padding: 3px 7px;
        color: #333;
        border-radius: 5px;
        border-collapse: collapse;
        font-size: 14px;
        margin: 2px;
        height: 30px;
        width: 155px;
        font-family: "PT Sans", Helvetica, Arial, sans-serif;
        color: white;
    }
</style>


<script>
    function nextPage(a, targetPage) {
        var tr = document.getElementById(a);
        var td = tr.getElementsByTagName("td");
        document.location.href = targetPage + '?a=' + td[0].innerHTML;
    }
</script>
</html>