package com.java.services;


import com.java.dao.GroupDAO;
import com.java.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupDAO groupDAO;


    @Override
    public void add(Group group) {
        groupDAO.add(group);
    }

    @Override
    public List<Group> getAll() {
        return groupDAO.getAll();
    }

    @Override
    public Group getByID(int id) {
        return groupDAO.getByID(id);
    }

    @Override
    public void update(Group group) {
        groupDAO.update(group);
    }

    @Override
    public void delete(Group group) {
        groupDAO.delete(group);
    }
}
