package com.java.services;


import com.java.domain.Client;
import com.java.domain.Task;
import com.java.domain.TaskTrigger;
import com.java.domain.TransportTask;

import java.util.List;

public interface TaskTriggerService {
    //create
    void add(TaskTrigger taskTrigger);

    //read
    List<TransportTask> getAll();

    TaskTrigger getByID(int id);

    //update
    void update(TaskTrigger taskTrigger);

    //delete
    void delete(TaskTrigger taskTrigger);

    List<TransportTask> getAllClientName();

    List<TransportTask> getAllTaskName();
}
