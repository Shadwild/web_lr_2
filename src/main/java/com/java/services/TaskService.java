package com.java.services;


import com.java.domain.Task;
import com.java.domain.TransportTask;

import java.sql.SQLException;
import java.util.List;

public interface TaskService {
    //create
    void add(Task task) ;
    //read
    List<TransportTask> getAll() ;
    Task getByID(int id) ;
    //update
    void update(Task task);
    //delete
    void delete(Task task) ;

    List<TransportTask> getAllClientName();
    List<TransportTask> getAllGroupName() ;
    List<TransportTask> getGroupByParentGroup(int numberParentGroup) ;
}
