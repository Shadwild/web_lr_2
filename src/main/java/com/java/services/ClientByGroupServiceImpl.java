package com.java.services;


import com.java.dao.ClientByGroupDAO;
import com.java.domain.ClientByGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class ClientByGroupServiceImpl implements ClientByGroupService {
   // private final DBConnector dbConnector;
    @Autowired
    private ClientByGroupDAO clientByGroupDAO;

   /* @Autowired
    public ClientByGroupServiceImpl(DBConnector connector) throws SQLException {
        this.dbConnector = connector;

    }*/

    @Override
    public void add(ClientByGroup clientByGroup) throws SQLException {
      /*  Connection connection = null;
        PreparedStatement preparedStatement = null;
        //ID автогенерация
        String sql = "INSERT INTO client_by_group (client_id, group_id) VALUES(?,?);";
        try {
            connection = dbConnector.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, clientByGroup.getClientId());
            preparedStatement.setInt(2, clientByGroup.getGroupId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }*/

        clientByGroupDAO.add(clientByGroup);
    }

    @Override
    public List<ClientByGroup> getAll() throws SQLException {
   /*     List<ClientByGroup> clientByGroupArrayList = new ArrayList<>();
        Connection connection = null;

        String sql = "SELECT client_by_group.client_id, client_by_group.group_id FROM client_by_group;";

        Statement statement = null;
        try {
            connection = dbConnector.getConnection();
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                ClientByGroup clientByGroup = new ClientByGroup();
                clientByGroup.setClientId(resultSet.getInt("client_id"));
                clientByGroup.setGroupId(resultSet.getInt("group_id"));
                clientByGroupArrayList.add(clientByGroup);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return clientByGroupArrayList;
        */
        return clientByGroupDAO.getAll();
    }

    /*@Override
    public void update(ClientByGroup clientByGroup) {

    }*/

    @Override
    public void delete(ClientByGroup clientByGroup) throws SQLException {
        /*PreparedStatement preparedStatement = null;

        Connection connection = null;
        String sql = "DELETE FROM client_by_group WHERE client_by_group.client_id=? AND client_by_group.group_id=?";

        try {
            connection = dbConnector.getConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, clientByGroup.getClientId());
            preparedStatement.setInt(2, clientByGroup.getGroupId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }*/
        clientByGroupDAO.delete(clientByGroup);
    }

}
