package com.java.services;


import com.java.dao.TaskTriggerDAO;
import com.java.domain.Client;
import com.java.domain.Task;
import com.java.domain.TaskTrigger;
import com.java.domain.TransportTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TaskTriggerServiceImpl implements TaskTriggerService {
    @Autowired
    private TaskTriggerDAO taskTriggerDAO;


    @Override
    public void add(TaskTrigger taskTrigger) {
        taskTriggerDAO.add(taskTrigger);
    }

    @Override
    public List<TransportTask> getAll() {
        List<TransportTask> transportTaskList = new ArrayList<>();

        for (TaskTrigger taskTrigger : taskTriggerDAO.getAll()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setTriggerId(taskTrigger.getTriggerId());
            transportTask.setTaskName(taskTrigger.getTask().getTaskName());
            transportTask.setCounter(taskTrigger.getCounter());
            transportTask.setClientLogin(taskTrigger.getClientTrigger().getLogin());
            transportTaskList.add(transportTask);
        }
        return transportTaskList;
    }

    @Override
    public TaskTrigger getByID(int id) {
        return taskTriggerDAO.getByID(id);
    }

    @Override
    public void update(TaskTrigger taskTrigger) {
        taskTriggerDAO.update(taskTrigger);
    }

    @Override
    public void delete(TaskTrigger taskTrigger) {
        taskTriggerDAO.delete(taskTrigger);
    }

    @Override
    public List<TransportTask> getAllClientName() {
        List<TransportTask> transportTasks = new ArrayList<>();
        for (Client client : taskTriggerDAO.getAllClientName()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setClientId(client.getClientId());
            transportTask.setClientLogin(client.getLogin());
            transportTasks.add(transportTask);
        }
        return transportTasks;
    }

    @Override
    public List<TransportTask> getAllTaskName() {
        List<TransportTask> transportTasks = new ArrayList<>();
        for (Task task : taskTriggerDAO.getAllTaskName()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setTaskId(task.getTaskId());
            transportTask.setTaskName(task.getTaskName());
            transportTasks.add(transportTask);
        }
        return transportTasks;
    }
}
