package com.java.services;


import com.java.dao.TaskDAO;
import com.java.domain.Client;
import com.java.domain.Group;
import com.java.domain.Task;
import com.java.domain.TransportTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskServiceImpl implements TaskService {


    @Autowired
    private TaskDAO taskDAO;

    //Если добавляемый таск для одного человека (личный) то clientID=0 т.к. в бд null
    @Override
    public void add(Task task) {
        taskDAO.add(task);
    }

    //при чтении полей с null в int переменную значение = 0
    @Override
    public List<TransportTask> getAll() {
       List<TransportTask> taskList = new ArrayList<>();

        for (Task task : taskDAO.getAll()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setTaskId(task.getTaskId());
           // transportTask.setClientLogin(task.getClientLogin());
            transportTask.setGroupName(task.getGroupName().getNameGroup());
            transportTask.setClientLogin(task.getClient().getLogin());
            transportTask.setTaskName(task.getTaskName());
            transportTask.setDateTime(task.getDateTime());
            transportTask.setTaskDescr(task.getTaskDescr());
            taskList.add(transportTask);
        }
        return taskList;
    }

    @Override
    public Task getByID(int id) {
        return taskDAO.getByID(id);
    }

    @Override
    public void update(Task task) {
        taskDAO.update(task);

    }

    @Override
    public void delete(Task task) {
        taskDAO.delete(task);

    }

    @Override
    public List<TransportTask> getAllClientName() {
        List<TransportTask> transportTasks = new ArrayList<>();
        for (Client client : taskDAO.getAllClientName()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setClientId(client.getClientId());
            transportTask.setClientLogin(client.getLogin());
            transportTasks.add(transportTask);
        }
        return transportTasks;
    }

    @Override
    public List<TransportTask> getAllGroupName() {
        List<TransportTask> transportTasks = new ArrayList<>();
        for (Group group : taskDAO.getAllGroupName()) {
            TransportTask transportTask = new TransportTask();
            transportTask.setGroupId(group.getGroupId());
            transportTask.setGroupName(group.getNameGroup());
            transportTasks.add(transportTask);
        }
        return transportTasks;
    }

    @Override
    public List<TransportTask> getGroupByParentGroup(int numberParentGroup) {
       /* List<TransportTask> groupList = new ArrayList<>();
        Connection connection = null;



        /*String sql = "with recursive temp1 as(\n" +
                "select distinct task.task_id,client.login,user_group.name,user_group.group_id,user_group.parent_id,task.task_name,task.task_datetime,task.task_descr from user_group,task,client  where user_group.parent_id =? and user_group.group_id=task.group_id\n" +
                "union\n" +
                "select distinct task.task_id,client.login,user_group.name,user_group.group_id,user_group.parent_id,task.task_name,task.task_datetime,task.task_descr from user_group,client,task  inner join temp1 on(temp1.group_id=task.group_id) where  user_group.group_id=task.group_id)\n" +
                "select * from temp1";*/
     /*   String sql = "WITH RECURSIVE r AS ( " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group, task, client " +
                "WHERE( " +
                "user_group.parent_id = ? " +
                "AND task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id) " +
                "UNION " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group,task,client,r " +
                "WHERE( " +
                "task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id " +
                "AND user_group.parent_id = r.group_id) " +
                ") " +
                "SELECT * FROM r;";

        PreparedStatement statement = null;
        try {
            connection = dbConnector.getConnection();
            statement = connection.prepareStatement(sql);

            statement.setInt(1, numberParentGroup);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                TransportTask task = new TransportTask();
                task.setTaskId(resultSet.getInt("task_id"));
                task.setClientLogin(resultSet.getString("login"));
                task.setGroupName(resultSet.getString("name"));
                task.setTaskName(resultSet.getString("task_name"));
                task.setDateTime(resultSet.getTimestamp("task_datetime"));
                task.setTaskDescr(resultSet.getString("task_descr"));
                groupList.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return groupList;
        */
        return taskDAO.getGroupByParentGroup(numberParentGroup);
    }
}
