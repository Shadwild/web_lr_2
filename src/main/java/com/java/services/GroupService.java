package com.java.services;


import com.java.domain.Group;

import java.sql.SQLException;
import java.util.List;

public interface GroupService {
    //create
    void add(Group group);

    //read
    List<Group> getAll();

    Group getByID(int id);

    //update
    void update(Group group);

    //delete
    void delete(Group group);
}
