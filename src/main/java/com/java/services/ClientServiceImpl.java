package com.java.services;


import com.java.dao.ClientDAO;
import com.java.domain.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientDAO clientDAO;


    public void add(Client client) {
        clientDAO.add(client);
    }

    public List<Client> getAll() {
        return clientDAO.getAll();
    }

    public Client getByID(int id) {
        return clientDAO.getByID(id);
    }


    public void update(Client client) {
        clientDAO.update(client);
    }

    public void delete(Client client) {
        clientDAO.delete(client);
    }

}
