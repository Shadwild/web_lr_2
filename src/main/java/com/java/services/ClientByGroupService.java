package com.java.services;



import com.java.domain.ClientByGroup;

import java.sql.SQLException;
import java.util.List;

public interface ClientByGroupService {
    //create
    void add(ClientByGroup clientByGroup) throws SQLException;
    //read
    List<ClientByGroup> getAll() throws SQLException;
    //ClientByGroup getByClientID(int id);//id группы или.. а надо ли вообще
    //update //тоже не знаю
   // void updateByClientID(ClientByGroup clientByGroup);
  //  void update(ClientByGroup clientByGroup);
    //delete
    void delete(ClientByGroup clientByGroup) throws SQLException;
}
