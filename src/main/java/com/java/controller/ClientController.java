package com.java.controller;

import com.java.domain.Client;
import com.java.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class ClientController {

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "ClientShow", method = RequestMethod.GET)
    public ModelAndView clientList() {
        List<Client> list = clientService.getAll();
        return new ModelAndView("ClientShow", "clients", list);
    }


    @RequestMapping(value = "ClientShow", method = RequestMethod.POST)
    public ModelAndView clientShow(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Client client = new Client();
        client.setClientId(Integer.parseInt(request.getParameter("clientId")));
        client.setLogin(request.getParameter("login"));
        client.setPassword(request.getParameter("password"));
        List<Client> list = null;
        if (request.getParameter("add") != null) {
            clientService.add(client);
        }
        if (request.getParameter("upd") != null) {
            clientService.update(client);
        }
        if (request.getParameter("del") != null) {
            clientService.delete(client);
        }
        list = clientService.getAll();
        return new ModelAndView("ClientShow", "clients", list);
    }



    @RequestMapping(value = "ClientEdit",method = RequestMethod.GET)
    public ModelAndView clientEdit(HttpServletRequest request, HttpServletResponse response) {
        Client client = null;
        client = clientService.getByID(Integer.parseInt(request.getParameter("a")));
        request.setAttribute("client",client);
        return new ModelAndView("ClientEdit", "clients", client);
    }

}
