package com.java.controller;

import com.java.domain.Client;
import com.java.domain.Group;
import com.java.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Игорь on 06.05.2018.
 */
@Controller
public class GroupController {

    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "GroupShow", method = RequestMethod.GET)
    public ModelAndView groupList() {
        List<Group> all = groupService.getAll();
        return new ModelAndView("GroupShow", "groups", all);
    }

    @RequestMapping(value = "GroupShow", method = RequestMethod.POST)
    public ModelAndView groupShow(HttpServletResponse response, HttpServletRequest request) {
        Group group = new Group();
        List<Group> list = null;

        group.setGroupId(Integer.parseInt(request.getParameter("groupId")));
        group.setParentGroupId(Integer.parseInt(request.getParameter("parentGroupId")));
        group.setNameGroup(request.getParameter("nameGroup"));

        if (request.getParameter("add") != null) {
            groupService.add(group);
        }
        if (request.getParameter("upd") != null) {
            groupService.update(group);
        }
        if (request.getParameter("del") != null) {
            groupService.delete(group);
        }
        list = groupService.getAll();
        return new ModelAndView("GroupShow", "groups", list);
    }

    @RequestMapping(value = "GroupEdit", method = RequestMethod.GET)
    public ModelAndView groupEdit(HttpServletRequest request, HttpServletResponse response) {
        Group group = null;
        group = groupService.getByID(Integer.parseInt(request.getParameter("a")));
        return new ModelAndView("GroupEdit", "group", group);
    }
}
