package com.java.controller;

import com.java.domain.Group;
import com.java.domain.TaskTrigger;
import com.java.domain.TransportTask;
import com.java.services.ClientService;
import com.java.services.GroupService;
import com.java.services.TaskService;
import com.java.services.TaskTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Игорь on 07.05.2018.
 */
@Controller
public class TaskTriggerController {
    @Autowired
    private TaskTriggerService taskTriggerService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "TaskTriggerShow", method = RequestMethod.GET)
    public ModelAndView taskTriggerShow() {
        List<TransportTask> all = taskTriggerService.getAll();
        return new ModelAndView("TaskTriggerShow", "triggers", all);
    }

    @RequestMapping(value = "TaskTriggerShow", method = RequestMethod.POST)
    public ModelAndView taskTrigger(HttpServletResponse response, HttpServletRequest request) {
        TaskTrigger taskTrigger = new TaskTrigger();
        List<TransportTask> list = null;

        taskTrigger.setTriggerId(Integer.parseInt(request.getParameter("triggerId")));
        // taskTrigger.setTaskId(Integer.parseInt(request.getParameter("nameTask")));
        // taskTrigger.setClientId(Integer.parseInt(request.getParameter("nameClient")));
        taskTrigger.setClientTrigger(clientService.getByID(Integer.parseInt(request.getParameter("nameClient"))));
        taskTrigger.setTask(taskService.getByID(Integer.parseInt(request.getParameter("nameTask"))));
        taskTrigger.setCounter(Integer.parseInt(request.getParameter("counter")));

        if (request.getParameter("add") != null) {
            taskTriggerService.add(taskTrigger);
        }
        if (request.getParameter("upd") != null) {
            taskTriggerService.update(taskTrigger);
        }
        if (request.getParameter("del") != null) {
            taskTriggerService.delete(taskTrigger);
        }
        list = taskTriggerService.getAll();
        return new ModelAndView("TaskTriggerShow", "triggers", list);
    }

    @RequestMapping(value = "TaskTriggerEdit", method = RequestMethod.GET)
    public ModelAndView taskTriggerEdit(HttpServletRequest request, HttpServletResponse response) {
        TaskTrigger taskTrigger = null;

        taskTrigger = taskTriggerService.getByID(Integer.parseInt(request.getParameter("a")));
        List<TransportTask> allClientName = taskTriggerService.getAllClientName();
        List<TransportTask> allTaskName = taskTriggerService.getAllTaskName();
        request.setAttribute("nameClient", allClientName);
        request.setAttribute("nameTask", allTaskName);
        return new ModelAndView("TaskTriggerEdit", "trigger", taskTrigger);
    }
}
