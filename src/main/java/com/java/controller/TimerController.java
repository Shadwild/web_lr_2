package com.java.controller;

import com.java.dao.TaskDAO;
import com.java.domain.Task;
import com.java.timer.WDTimer;
import com.java.timer.timerDao.TimerDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Игорь on 26.05.2018.
 */
@Controller
public class TimerController {

    @Autowired
    private TaskDAO dao;

    public TimerDaoImpl taskDAO = new TimerDaoImpl();
    private WDTimer timer = WDTimer.getInstance();
    //public DefaultListModel listModel=null;//модель списка вьюхи

    private Observable observableController = new Observable() {
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    };

    public void addObserver(Observer o) {
        observableController.addObserver(o);
    }

    public void deleteObserver(Observer o) {
        observableController.deleteObserver(o);
    }

    public TimerController() {

    }

    public void addTask(Task task) {
        taskDAO.addTask(task);

        taskDAO.recount();
        sort();
       // timer.setWaitTime(taskDAO.getTask(0).getSecondsTo());
       /* timer.setTask(() -> {
            observableController.notifyObservers(new Object[]{6, null, null});
        });*/
        timer.setTask(task);

    }

    public void loadTask() {
        taskDAO.loadTaskFromDB(dao.getAll());
        taskDAO.recount();
        searchFutureTask();
        sort();
        setTask();
    }

    public void searchFutureTask() {
        Iterator<Task> iterator=taskDAO.getTaskArrayList().iterator();

        while (iterator.hasNext()){
            if(iterator.next().getSecondsTo()<=0){
                iterator.remove();
            }
        }

       /* for (Task task : taskArrayList) {
            if (task.getSecondsTo()<= 0) {
                //taskDAO.getTaskArrayList().remove(task);
                taskArrayList.remove(task);
            }
        }*/


    }

    public void setTask(){
        for (Task task:taskDAO.getTaskArrayList()) {
            timer.setTask(task);
        }
    }

    public void updateTask(int id, Task task) {
        timer.cancelTask();
        taskDAO.updateTask(id, task);
       // observableController.notifyObservers(new Object[]{2, id, task.getTaskName()});
        //listModel.set(id, task.getName());
        //taskDAO.recount(LocalDateTime.now());
        sort();
        timer.setWaitTime(taskDAO.getTask(0).getSecondsTo());
        //  timer.setTask(() -> observableController.notifyObservers(new Object[]{6, null, null}));
        //timer.setTask(task);

    }

    public void removeTask(int id) {
        //  taskDAO.removeTask(id);
        //listModel.remove(id);
        observableController.notifyObservers(new Object[]{3, id, null});
        if (id != 0) {
            //taskDAO.recount(LocalDateTime.now());
            taskDAO.sort();
            timer.setWaitTime(taskDAO.getTask(0).getSecondsTo());
            /*timer.setTask(() -> {observableController.notifyObservers(new Object[]{6, null, null});
            });*/
        }
    }

    public Task getTask(int index) {
        return taskDAO.getTask(index);
    }

    public void sort() {
        taskDAO.sort();
        // observableController.notifyObservers(new Object[]{4, null, null});
    }
}
