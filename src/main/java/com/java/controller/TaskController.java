package com.java.controller;

import com.java.domain.Task;
import com.java.domain.TransportTask;
import com.java.services.ClientService;
import com.java.services.GroupService;
import com.java.services.TaskService;
import com.java.util.Temporator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Created by Игорь on 07.05.2018.
 */

@Controller
public class TaskController {



    @Autowired
    private TimerController timerController;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private GroupService groupService;


    @RequestMapping()
    public void loadAllTaskFromDB() {
        timerController.loadTask();
        // WDTimer wdTimer=new WDTimer();

    }

    @RequestMapping(value = "TaskShow", method = RequestMethod.GET)
    public ModelAndView getAll(HttpServletRequest request) {
        List<TransportTask> all = taskService.getAll();
        if (request.getParameter("found") != null) {
            all = taskService.getGroupByParentGroup(Integer.parseInt(request.getParameter("nameGroup")));
        }
        return new ModelAndView("TaskShow", "tasks", all);
    }

    @RequestMapping(value = "TaskShow", method = RequestMethod.POST)
    public ModelAndView groupShow(HttpServletResponse response, HttpServletRequest request) {
        Task task = new Task();
        List<TransportTask> list = null;
        task.setTaskId(Integer.parseInt(request.getParameter("taskId")));
        task.setClient(clientService.getByID(Integer.parseInt(request.getParameter("nameClient"))));
        task.setGroupName(groupService.getByID(Integer.parseInt(request.getParameter("nameGroup"))));
        task.setTaskName(request.getParameter("taskName"));
        task.setDateTime(Temporator.stringToDate(request.getParameter("dateTime")));
        task.setTaskDescr(request.getParameter("taskDescr"));

        if (request.getParameter("add") != null) {
            taskService.add(task);
            // timerDao.addTask(task);
            timerController.addTask(task);
        }
        if (request.getParameter("upd") != null) {
            taskService.update(task);
            //  timerDao.updateTask(0, task);
        }
        if (request.getParameter("del") != null) {
            taskService.delete(task);
            //  timerDao.removeTask();
        }
        if (request.getParameter("found") != null) {
            String attribute = request.getParameter("name");
        }
        list = taskService.getAll();
        return new ModelAndView("TaskShow", "tasks", list);
    }

    @RequestMapping(value = "TaskEdit", method = RequestMethod.GET)
    public ModelAndView groupEdit(HttpServletRequest request, HttpServletResponse response) {
        List<TransportTask> transportClientName;
        List<TransportTask> transportGroupName;
        Task task = null;

        task = taskService.getByID(Integer.parseInt(request.getParameter("a")));
        transportClientName = taskService.getAllClientName();
        transportGroupName = taskService.getAllGroupName();

        request.setAttribute("nameClient", transportClientName);
        request.setAttribute("nameGroup", transportGroupName);
        return new ModelAndView("TaskEdit", "task", task);
    }


   /* @RequestMapping
    public void alert(Task task, HttpServletRequest request) {
        request.setAttribute("qwe", "1");
    }*/
}
