package com.java.dao;


import com.java.domain.Client;
import com.java.domain.Group;
import com.java.domain.Task;
import com.java.domain.TransportTask;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Игорь on 22.04.2018.
 */
@Repository
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    private SessionFactory sessionFactory;

    //Если добавляемый таск для одного человека (личный) то clientID=0 т.к. в бд null
    @Override
    public void add(Task task) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(task);
        tx.commit();
        session.close();
    }

    //при чтении полей с null в int переменную значение = 0
    @Override
    public List<Task> getAll() {
        Session session = sessionFactory.openSession();
      Query query=session.createQuery("from Task ");
        List<Task> list = query.list();
        session.close();
        return list;
    }

    @Override
    public Task getByID(int id) {
        Session session = sessionFactory.openSession();
        Task load = session.get(Task.class, id);
        session.close();
        return load;
    }

    @Override
    public void update(Task task) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(task);
        tx.commit();
        session.close();

    }

    @Override
    public void delete(Task task) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(task);
        tx.commit();
        session.close();
    }

    @Override
    public List<Client> getAllClientName() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("select distinct c from Client c").list();
        session.close();
        return list;
    }

    @Override
    public List<Group> getAllGroupName() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("select distinct g from Group g").list();
        session.close();
        return list;
    }

    @Override
    public List<TransportTask> getGroupByParentGroup(int numberParentGroup) {
       /* List<TransportTask> groupList = new ArrayList<>();
        Connection connection = null;



        /*String sql = "with recursive temp1 as(\n" +
                "select distinct task.task_id,client.login,user_group.name,user_group.group_id,user_group.parent_id,task.task_name,task.task_datetime,task.task_descr from user_group,task,client  where user_group.parent_id =? and user_group.group_id=task.group_id\n" +
                "union\n" +
                "select distinct task.task_id,client.login,user_group.name,user_group.group_id,user_group.parent_id,task.task_name,task.task_datetime,task.task_descr from user_group,client,task  inner join temp1 on(temp1.group_id=task.group_id) where  user_group.group_id=task.group_id)\n" +
                "select * from temp1";*/
    /*    String sql = "WITH RECURSIVE r AS ( " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group, task, client " +
                "WHERE( " +
                "user_group.parent_id = ? " +
                "AND task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id) " +
                "UNION " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group,task,client,r " +
                "WHERE( " +
                "task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id " +
                "AND user_group.parent_id = r.group_id) " +
                ") " +
                "SELECT * FROM r;";

        PreparedStatement statement = null;
        try {
            connection = dbConnector.getConnection();
            statement = connection.prepareStatement(sql);

            statement.setInt(1, numberParentGroup);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                TransportTask task = new TransportTask();
                task.setTaskId(resultSet.getInt("task_id"));
                task.setClientLogin(resultSet.getString("login"));
                task.setGroupName(resultSet.getString("name"));
                task.setTaskName(resultSet.getString("task_name"));
                task.setDateTime(resultSet.getTimestamp("task_datetime"));
                task.setTaskDescr(resultSet.getString("task_descr"));
                groupList.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return groupList;*/
        Session session = sessionFactory.openSession();
        List<TransportTask> transportTasks = new ArrayList<>();

        SQLQuery sqlQuery = session.createSQLQuery("WITH RECURSIVE r AS ( " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group, task, client " +
                "WHERE( " +
                "user_group.parent_id = ? " +
                "AND task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id) " +
                "UNION " +
                "SELECT " +
                "task.task_id, " +
                "client.login, " +
                "user_group.name, " +
                "user_group.group_id, " +
                "user_group.parent_id, " +
                "task.task_name, " +
                "task.task_datetime, " +
                "task.task_descr " +
                "FROM user_group,task,client,r " +
                "WHERE( " +
                "task.group_id = user_group.group_id " +
                "AND task.client_id = client.client_id " +
                "AND user_group.parent_id = r.group_id) " +
                ") " +
                "SELECT * FROM r;");
        sqlQuery.setParameter(0, numberParentGroup);

        List<Object[]> list = sqlQuery.list();
        for (Object[] arr : list) {
            TransportTask transportTask = new TransportTask();
            transportTask.setTaskId((Integer) arr[0]);
            transportTask.setClientLogin((String) arr[1]);
            transportTask.setGroupName((String) arr[2]);
            transportTask.setGroupId((Integer) arr[3]);
            transportTask.setParentId((Integer) arr[4]);
            transportTask.setTaskName((String) arr[5]);
          //  transportTask.setDateTime((Date) arr[6]);
            transportTask.setTaskDescr((String) arr[7]);

            transportTasks.add(transportTask);

        }
        return transportTasks;
    }
}
