package com.java.dao;


import com.java.domain.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by Игорь on 22.04.2018.
 */


@Repository
public class ClientDAOImpl implements ClientDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public void add(Client client) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        tx = session.beginTransaction();
        session.save(client);
        tx.commit();
        session.close();
    }

    public List<Client> getAll() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("from Client").list();
        session.close();
        return list;
    }

    public Client getByID(int id) {
        Session session = sessionFactory.openSession();
        Client client = session.get(Client.class, id);
        session.close();
        return client;
    }

    //может сделать чтобы id отдельным параметром
    public void update(Client client) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        tx = session.beginTransaction();
        session.update(client);
        tx.commit();
        session.close();

    }

    public void delete(Client client) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        tx = session.beginTransaction();
        session.delete(client);
        tx.commit();
        session.close();

    }


}
