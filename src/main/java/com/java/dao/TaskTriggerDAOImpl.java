package com.java.dao;


import com.java.domain.Client;
import com.java.domain.Task;
import com.java.domain.TaskTrigger;
import com.java.domain.TransportTask;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Игорь on 22.04.2018.
 */
@Repository
public class TaskTriggerDAOImpl implements TaskTriggerDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void add(TaskTrigger taskTrigger) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(taskTrigger);
        tx.commit();
        session.close();
    }

    @Override
    public List<TaskTrigger> getAll() {
        Session session = sessionFactory.openSession();
        List query=session.createQuery("from TaskTrigger ").list();
        session.close();
        return query;
    }

    @Override
    public TaskTrigger getByID(int id) {
        Session session = sessionFactory.openSession();;
        TaskTrigger load = session.get(TaskTrigger.class, id);
        session.close();
        return load;
    }

    @Override
    public void update(TaskTrigger taskTrigger) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(taskTrigger);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(TaskTrigger taskTrigger) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(taskTrigger);
        tx.commit();
        session.close();
    }

    @Override
    public List<Client> getAllClientName() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("select distinct c from Client c").list();
        session.close();
        return list;
    }

    @Override
    public List<Task> getAllTaskName() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("select distinct t from Task t").list();
        session.close();
        return list;
    }
}
