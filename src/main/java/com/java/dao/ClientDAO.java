package com.java.dao;

import com.java.domain.Client;

import java.util.List;

public interface ClientDAO {
    //create
    void add(Client client);

    //read
    List<Client> getAll();

    Client getByID(int id);

    //update
    void update(Client client);

    //delete
    void delete(Client client);


}
