package com.java.dao;


import com.java.domain.Group;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Игорь on 22.04.2018.
 */
@Repository
public class GroupDAOImpl implements GroupDAO {

    @Autowired
    private SessionFactory sessionFactory;

    //таблица иерархическая, поэтому первый небычный.
    @Override
    public void add(Group group) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(group);
        tx.commit();
        session.close();
    }

    @Override
    public List<Group> getAll() {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("from Group").list();
        session.close();
        return list;
    }


    @Override
    public Group getByID(int id) {
        Session session = sessionFactory.openSession();
        //  Group group = session.load(Group.class, id);
        Group group = session.get(Group.class, id);
        session.close();
        return group;
    }

    @Override
    public void update(Group group) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(group);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Group group) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(group);
        tx.commit();
        session.close();
    }
}
