package com.java.dao;


import com.java.domain.Group;

import java.sql.SQLException;
import java.util.List;

public interface GroupDAO {
    //create
    void add(Group group);

    //read
    List<Group> getAll();

    Group getByID(int id);

    //update
    void update(Group group);

    //delete
    void delete(Group group);
}
