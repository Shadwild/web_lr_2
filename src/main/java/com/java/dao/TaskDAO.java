package com.java.dao;


import com.java.domain.Client;
import com.java.domain.Group;
import com.java.domain.Task;
import com.java.domain.TransportTask;

import java.sql.SQLException;
import java.util.List;

public interface TaskDAO {
    //create
    void add(Task task) ;

    //read
    List<Task> getAll() ;

    Task getByID(int id);

    //update
    void update(Task task) ;

    //delete
    void delete(Task task);

    List<Client> getAllClientName() ;

    List<Group> getAllGroupName() ;

    List<TransportTask> getGroupByParentGroup(int numberParentGroup);
}
