package com.java.dao;


import com.java.domain.Client;
import com.java.domain.Task;
import com.java.domain.TaskTrigger;
import com.java.domain.TransportTask;

import java.sql.SQLException;
import java.util.List;

public interface TaskTriggerDAO {
    //create
    void add(TaskTrigger taskTrigger);

    //read
    List<TaskTrigger> getAll();

    TaskTrigger getByID(int id);

    //update
    void update(TaskTrigger taskTrigger);

    //delete
    void delete(TaskTrigger taskTrigger);

    List<Client> getAllClientName();

    List<Task> getAllTaskName();

}
