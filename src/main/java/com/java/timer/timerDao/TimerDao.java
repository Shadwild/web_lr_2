package com.java.timer.timerDao;

import com.java.domain.Task;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Игорь on 26.05.2018.
 */
public interface TimerDao {
    void addTask(Task task);
    void updateTask(int id, Task task);
    void removeTask(Task task);
    Task getTask(int id);
    List<Task> getTaskList();
    void sort();
    void recount();
    void loadTaskFromDB(List<Task> tasks);
}
