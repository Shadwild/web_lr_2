package com.java.timer.timerDao;

import com.java.domain.Task;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Игорь on 26.05.2018.
 */
@Component
public class TimerDaoImpl implements TimerDao {
    private List<Task> taskArrayList=new ArrayList<>();

    @Override
    public void addTask(Task task) {
        taskArrayList.add(task);
    }

    @Override
    public void updateTask(int id, Task task) {
        taskArrayList.set(id, task);
    }

    @Override
    public void removeTask(Task task) {
        taskArrayList.remove(task);
    }

    @Override
    public Task getTask(int id) {
        return taskArrayList.get(id);
    }

    @Override
    public List getTaskList() {
        return taskArrayList;
    }


    @Override
    public void sort()
    {
        Collections.sort(taskArrayList, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });
    }
    @Override
    public void recount()
    {

        for (Task task:taskArrayList)
        {
           task.setSecondsTo(task.recount());

        }
    }

    @Override
    public void loadTaskFromDB(List<Task> tasks) {
        taskArrayList=tasks;
    }

    public List<Task> getTaskArrayList() {
        return taskArrayList;
    }
}
