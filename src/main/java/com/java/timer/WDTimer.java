package com.java.timer;

import com.java.domain.Task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class WDTimer {
    private static ScheduledExecutorService service;
    private static long waitTime; //в секундах.
    private static ScheduledFuture future;
    private static volatile WDTimer timer;

    private WDTimer() {
        this.service = Executors.newSingleThreadScheduledExecutor();
    }

    private WDTimer(long waitTime) {
        this.waitTime = waitTime;
        this.service = Executors.newSingleThreadScheduledExecutor();
    }

    public static WDTimer getInstance() {
        if (timer == null) {
            synchronized (WDTimer.class) {
                if (timer == null) {
                    timer = new WDTimer(0);
                }
            }
        }
        return timer;
    }

    public long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }

    public void setTask()//дефолтная задача
    {
        future = service.schedule(() -> {
            System.out.println("Запланированное событие");
        }, waitTime, TimeUnit.SECONDS);
    }

    public void setTask(TaskActivity task)//установка пользовательской задачи
    {
        future = service.schedule(() -> {
            task.task();
        }, waitTime, TimeUnit.SECONDS);
    }

    public void setTask(Task task) {
        ScheduledExecutorService executorService=Executors.newSingleThreadScheduledExecutor();
        executorService.schedule(task,task.getSecondsTo(),TimeUnit.SECONDS);
        try {
            service.awaitTermination(waitTime + 5, TimeUnit.SECONDS);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (!service.isTerminated()) {
                service.shutdownNow();
            }
        }
    }

    public void cancelTask() //отмена задачи.
    {
        if (!future.isDone()) {
            future.cancel(true);
        }
    }

    public void destroyTimer()//уничтожение исполнителя
    {
        if (!service.isShutdown()) {
            service.shutdown();
        }
    }
}