package com.java.domain;

import javax.persistence.*;
import java.io.Serializable;

//@Component
//@Lazy
@Entity
@Table(name = "client_by_group")
public class ClientByGroup implements Serializable {

    @Id
    @Column(name = "client_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clientId;

    @Column(name = "group_id")
    private int groupId;

    public ClientByGroup() {
    }

    public ClientByGroup(int clientId, int groupId) {
        this.clientId = clientId;
        this.groupId = groupId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClientByGroup{");
        sb.append("clientId=").append(clientId);
        sb.append(", groupId=").append(groupId);
        sb.append('}');
        return sb.toString();
    }
}
