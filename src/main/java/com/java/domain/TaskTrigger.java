package com.java.domain;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "task_trigger")
public class TaskTrigger implements Serializable {

    @Id
    @Column(name = "trigger_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int triggerId;

    @OneToOne
    @JoinColumn(name="task_id")
    private Task task;


    @OneToOne
    @JoinColumn(name = "client_id")
    private Client clientTrigger;


    @Column(name = "counter")
    private int counter;

    public TaskTrigger() {
    }

    public TaskTrigger(int triggerId, int taskId, int clientId, int counter) {
        this.triggerId = triggerId;
        this.counter = counter;
    }


    public int getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(int triggerId) {
        this.triggerId = triggerId;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }


    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Client getClientTrigger() {
        return clientTrigger;
    }

    public void setClientTrigger(Client clientTrigger) {
        this.clientTrigger = clientTrigger;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskTrigger{");
        sb.append("triggerId=").append(triggerId);
       // sb.append(", taskId=").append(taskId);
     //   sb.append(", clientId=").append(clientId);
        sb.append(", counter=").append(counter);
        sb.append('}');
        return sb.toString();
    }
}
