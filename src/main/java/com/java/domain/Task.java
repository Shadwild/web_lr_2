package com.java.domain;


import com.java.timer.MainTaskActivity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "task")
public class Task implements Serializable, Runnable {

    @Id
    @Column(name = "task_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int taskId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne(optional = false)
    @JoinColumn(name = "group_id")
    private Group groupName;


    @Column(name = "task_name")
    private String taskName;

    @Column(name = "task_datetime")
    private Date dateTime;

    @Column(name = "task_descr")
    private String taskDescr;

    @OneToOne(mappedBy = "task", fetch = FetchType.EAGER)
    private TaskTrigger taskTrigger;

    @Transient
    private long secondsTo;

    public Task() {
    }

    public Task(Client client, Group groupName, String taskName, Date dateTime, String taskDescr, TaskTrigger taskTrigger) {
        this.client = client;
        this.groupName = groupName;
        this.taskName = taskName;
        this.dateTime = dateTime;
        this.taskDescr = taskDescr;
        this.taskTrigger = taskTrigger;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Group getGroupName() {
        return groupName;
    }

    public void setGroupName(Group groupName) {
        this.groupName = groupName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;

        secondsTo = recount();
    }

    public String getTaskDescr() {
        return taskDescr;
    }

    public void setTaskDescr(String taskDescr) {
        this.taskDescr = taskDescr;
    }

    public long getSecondsTo() {
        return secondsTo;
    }

    public void setSecondsTo(long secondsTo) {
        this.secondsTo = secondsTo;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Task{");
        sb.append("taskId=").append(taskId);
        //  sb.append(", clientId=").append(clientId);
        //  sb.append(", groupId=").append(groupId);
        sb.append(", taskName='").append(taskName).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", taskDescr='").append(taskDescr).append('\'');
        sb.append("secondTo").append(secondsTo);
        sb.append('}');
        return sb.toString();
    }

    public long recount() {
        Date date = new Date();
        long result = (dateTime.getTime() - date.getTime()) / 1000;
        if (result < 0) return 0; //Math.abs(result);
        else return result;
    }

    @Override
    public void run() {
        System.out.println(toString());

    }
}
