package com.java.domain;

import java.io.Serializable;
import java.util.Date;



public class TransportTask implements Serializable {

    private int taskId;
    private int clientId;
    private int groupId;
    private String clientLogin;
    private String groupName;
    private String taskName;
    private Date dateTime;
    private String taskDescr;
    private int parentId;
    private int triggerId;
    private int counter;
    private Client client;

    public TransportTask() {
    }

    public TransportTask(int taskId, int clientId, int groupId, String clientLogin, String groupName, String taskName, Date dateTime, String taskDescr) {
        this.taskId = taskId;
        this.clientId = clientId;
        this.clientLogin = clientLogin;
        this.groupId = groupId;
        this.groupName = groupName;
        this.taskName = taskName;
        this.dateTime = dateTime;
        this.taskDescr = taskDescr;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getClientLogin() {
        return clientLogin;
    }

    public void setClientLogin(String clientLogin) {
        this.clientLogin = clientLogin;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getTaskDescr() {
        return taskDescr;
    }

    public void setTaskDescr(String taskDescr) {
        this.taskDescr = taskDescr;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }


    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(int triggerId) {
        this.triggerId = triggerId;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Task{");
        sb.append("taskId=").append(taskId);
        sb.append(", clientLogin=").append(clientLogin);
        sb.append(", groupName=").append(groupName);
        sb.append(", taskName='").append(taskName).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", taskDescr='").append(taskDescr).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
