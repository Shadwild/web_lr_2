package com.java.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * Created by Игорь on 06.05.2018.
 */
@Entity
@Table(name = "groups")
public class Group implements Serializable {

    @Id
    @Column(name = "group_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int groupId;

    @Column(name = "parent_group_id")
    private int parentGroupId;

    @Column(name = "name")
    private String nameGroup;

    @OneToMany(mappedBy = "groupName", fetch = FetchType.EAGER)
    private List<Task> taskList;

    public Group() {
    }

    public Group(int groupId, int parentGroupId, String nameGroup) {
        this.groupId = groupId;
        this.parentGroupId = parentGroupId;
        this.nameGroup = nameGroup;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(int parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }




    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Group{");
        sb.append("groupId=").append(groupId);
        sb.append(", parentGroupId=").append(parentGroupId);
        sb.append(", nameGroup='").append(nameGroup).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
