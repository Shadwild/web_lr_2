package com.java.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "client")
public class Client implements Serializable {

    @Id
    @Column(name = "client_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clientId;

    @Column(name = "login")
    private String login;

    @Column(name = "pass")
    private String password;

    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private List<Task> listTask;

    @OneToOne(mappedBy = "clientTrigger",fetch = FetchType.EAGER)
    private TaskTrigger taskTrigger;



    public Client() {
    }

    public Client(int clientId, String login, String password) {
        this.clientId = clientId;
        this.login = login;
        this.password = password;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Task> getListTask() {
        return listTask;
    }

    public void setListTask(List<Task> listTask) {
        this.listTask = listTask;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Client{");
        sb.append("clientId=").append(clientId);
        sb.append(", login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
